FROM node:17.6-alpine3.14 as ts-compiler
WORKDIR /usr/app
COPY package*.json ./
COPY tsconfig*.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM node:17.6-alpine3.14 as ts-remover
WORKDIR /usr/app
COPY --from=ts-compiler /usr/app/package*.json ./
COPY --from=ts-compiler /usr/app/dist ./
RUN npm install --only=production

FROM node:17.6-alpine3.14
USER 1000
WORKDIR /usr/app
COPY --from=ts-remover /usr/app ./
RUN echo "" >> .env
CMD ["node", "app.js"]