# Integração com SpaceX API

Este projeto foi criado para servir como back-end para integração com a API [SpaceX REST API](https://github.com/r-spacex/SpaceX-API).

Foi criado sob o template básico do projeto express e então alterado para Typescript.

## Para iniciar a aplicação localmente:

### `npm start`

Roda o app em ambiente de desenvolvimento,

É necessário ter um `Redis` Rodando para o cache, ou trocar a implementação para o node-cache. Há uma implementação funcional e seguindo a mesma interface `src/utils/node-cache.ts` no projeto.

A URL do Redis pode ser configurada por .env file ou utilizando variável de ambiente `REDIS_URL=`.

==Vale lembrar que para iniciar qualquer aplicação nodejs primeiro se deve instalar as dependências com comandos como o `npm i` ou `npm ci` e somente após isso deve ser utilizado o comando `npm start`==

O servidor roda na porta `3000` e isto pode ser alterado mudando a variável `PORT=` por ambiente ou `.env` file seguindo as [especificações](#environment) do arquivo.

Para facilitar o build existe um dockerfile e para infra básica local somente é necessário docker caso queira iniciar a aplicação.

O comando abaixo constrói um container que faz a compilação do typescript e gera uma versão compatível com nodejs para execução.

```
docker build -t spacex-api .
```

Após o comando acima podemos iniciar a infra com o comando:

```
docker-compose up
```

## Environment

O environment mantém um comportamento padrão caso ausente, em acessos não confidenciais, mas pode ser alterado por meio de um arqivo `.env` adicionado à raiz do projeto ou adicionando as variáveis de ambiente com as especificações abaixo:

```.env
LOG_LEVEL=development
LOG_FOLDER=.//logs//
LOG_FILE=ts-api-%DATE%.log
LOG_FILE_ERROR=ts-api-errors-%DATE%.log
PORT=3000
REDIS_URL=localhost
REDIS_PORT=6379
```

## Para iniciar a aplicação em ambiente de produção:

### `npm run build`

Constrói o app para implantação compilando o Typescript para Javascript,

### `npm run prod`

Executa a aplicação após o transpile de código diretamente em ambiente nodejs.

## Implementação:

O projeto foi pensado para fácil manutenção, com baixo acoplamento e dependendo em sua maioria de interfaces para conexões bibliotecas que dão a flexibilidad de trocar, por exempo o `Axios` para o `Fetch` ou até mesmo o módulo nativo `Https` do `Nodejs`. O código é simples e tira vantagens dos generics do Javascript para algumas integraçoes.

A API foi versionada para facilitar alterações que podem modificar o contrato sem problemas de modo que se mantenha o funcionamento e comportamento legados bem como a adição de novas features e alterações nas existentes.

## Fluxo

O fluxo compreende apenas as chamadas para o backEnd da API citada no início do texto, tratamento de exceções e transformação de dados. Foi adicionado um cache em memória com duração de 20 segundos para acompanhar o que se encontra na documentação da API consumida. Como não há interligações complexas,não houve uso de RESTFull com o padrão HATEOAS e seus benefícios de links para dados relacionados.

## Melhorias:

Há diversas melhorias possíveis, sempre. Mas algumas que posso destacar são a adição de mais testes.

## Endpoints:

Busca

Busca o próximo lançamento.

```
GET /v1/launches/next
```

Busca o último lançamento.

```
GET /v1/launches/latest
```

Busca os próximos lançamentos programados.

```
GET /v1/launches/upcoming
```

Busca os lançamentos passados.

```
GET /v1/launches/past
```

`Parâmetros:`

| Nome   | Obrigatório | valores                                                                                    |
| :----- | :---------: | :----------------------------------------------------------------------------------------- |
| fields |     não     | quaisquer atributos do [retorno](#retorno) desejados ex: `fields=name,rocket,flightNumber` |
| page   |     não     | página desejada                                                                            |
| limit  |     não     | quantidade de itens por página                                                             |
| sort   |     não     | ordenar por `field` ex.: `sort=-date_utc`                                                  |

### Docs:

Acesso ao SwaggerUI para documentação e tryout da API.

```
GET /v1/docs
```

## Retorno:

O retorno é basicamente um só, alternando apenas entre entidade singular ou array de entidade e segue o formato abaixo:

```json
{
    "name": "Starlink (v1.5)",
    "rocket": "5e9d0d95eda69973a809d1ec",
    "flightNumber": 151,
    "dateLocal": "2022-01-31T16:00:00-08:00",
    "smallImages": ["https://imgur.com/BrW201S.png"],
    "largeImages": ["https://imgur.com/573IfGk.png"],
    "youtubeId": "nnVOfKOzXHE"
}
```
