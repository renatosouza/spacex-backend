export class FileNotFoundError extends Error {
    constructor(file: string) {
        super(`${file} not founded!`);
    }
}
