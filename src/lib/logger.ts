import winston from "winston";
import DailyRotateFile from "winston-daily-rotate-file";

const LOG_LEVEL = process.env.LOG_LEVEL || "development";
const LOG_FOLDER = process.env.LOG_FOLDER || ".//logs//";
const LOG_FILE = process.env.LOG_FILE || "ts-api-%DATE%.log";
const LOG_ERROR_FILE = process.env.LOG_ERROR || "ts-api-errors-%DATE%.log";

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4
};

const level = () => {
    const env = process.env.NODE_ENV || "development";
    const isDevelopment = env === "development";
    return isDevelopment ? "debug" : "warn";
};

const colors = {
    error: "red",
    warn: "yellow",
    info: "green",
    http: "magenta",
    debug: "white"
};

// winston.addColors(colors);

const format = winston.format.combine(
    winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
    // winston.format.colorize({ all: true }),
    winston.format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
);

const rotateTransport = new DailyRotateFile({
    filename: LOG_FOLDER + LOG_FILE,
    datePattern: "DD-MM-YYYY",
    zippedArchive: true,
    maxSize: "20m",
    maxFiles: "14d",
    level: LOG_LEVEL
});

const transports = [
    new winston.transports.Console(),
    new winston.transports.File({
        filename: LOG_FOLDER + LOG_ERROR_FILE,
        level: "error"
    }),
    rotateTransport
];

const Logger = winston.createLogger({
    level: level(),
    levels,
    format,
    transports
});

export default Logger;
