export type Method =
    | "get"
    | "GET"
    | "delete"
    | "DELETE"
    | "head"
    | "HEAD"
    | "options"
    | "OPTIONS"
    | "post"
    | "POST"
    | "put"
    | "PUT"
    | "patch"
    | "PATCH"
    | "purge"
    | "PURGE"
    | "link"
    | "LINK"
    | "unlink"
    | "UNLINK";

export interface IRequestHeaders extends Record<string, string | number | boolean> {}

export interface IRequestConfig {
    url?: string;
    method?: Method;
    baseURL?: string;
    headers?: IRequestHeaders;
    params?: any;
}

export interface RequestException<T = any> extends Error {
    config: Partial<IRequestConfig>;
    code?: string;
    request?: any;
    response?: Partial<IHttpResponse<T>>;
    isAxiosError: boolean;
    toJSON: () => object;
}

export interface IHttpResponse<T = any> {
    data: T;
    status: number;
    statusText: string;
    headers: IRequestHeaders;
    config: IRequestConfig;
    request?: any;
}

export interface IHttp {
    get<T>(url: string, params?: {}, headers?: {}): Promise<IHttpResponse<T>>;
    post<T>(url: string, data?: any, params?: {}, headers?: {}): Promise<IHttpResponse<T>>;
    put<T>(url: string, data?: T | any, params?: {}, headers?: {}): Promise<IHttpResponse<T>>;
    delete<T>(url: string, params?: {}, headers?: {}): Promise<IHttpResponse<T>>;
}
