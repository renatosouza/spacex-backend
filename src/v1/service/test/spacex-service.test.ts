import SpaceXService, {
    SPACEX_API_ENDPOINT,
    SPACEX_API_LAUNCHES_LATEST,
    SPACEX_API_LAUNCHES_NEXT,
    SPACEX_API_LAUNCHES_PAST,
    SPACEX_API_LAUNCHES_PATH,
    SPACEX_API_VERSION
} from "../spacex-service/spacex_service";
import latestLaunch from "./fixtures/latest-launch.json";
import nextLaunch from "./fixtures/next-launch.json";
import pastLaunches from "./fixtures/past-launches.json";
import nock from "nock";
import { cache } from "../../../utils/cache";
import HttpClient from "../../../utils/http";

function setNockRequestScopes() {
    const nockScope = nock(
        `${SPACEX_API_ENDPOINT}${SPACEX_API_VERSION}${SPACEX_API_LAUNCHES_PATH}`
    );
    nockScope.get(SPACEX_API_LAUNCHES_NEXT).reply(200, nextLaunch);
    nockScope.get(SPACEX_API_LAUNCHES_LATEST).reply(200, latestLaunch);
    nockScope.post(SPACEX_API_LAUNCHES_PAST).reply(200, pastLaunches);
}

describe("SpaceX Service tests", () => {
    const service = new SpaceXService(new HttpClient(), cache);

    beforeAll(() => {
        cache.get = jest.fn().mockReturnValue(undefined);
        cache.set = jest.fn().mockReturnValue(true);
    });

    beforeEach(() => {
        if (process.env.NODE_ENV === "test:local") {
            setNockRequestScopes();
        }
    });

    it("should return next launch", async () => {
        const response = await service.getNextLaunch();
        expect(response.upcoming).toBeTruthy();
    });

    it("should return latest launch", async () => {
        const response = await service.getLatestLaunch();
        expect(response.upcoming).toBeFalsy();
    });

    it("should return past launches", async () => {
        const response = await service.getPastLaunches();
        expect(response.docs.length).toBeGreaterThan(0);
        expect(response.docs[0].payloads[0].launch.upcoming).toBeFalsy();
        expect(response.docs[0].payloads[0].launch.date_unix).toBeLessThan(Date.now());
    });

    it("should return past launches page 2 and custom query params", async () => {
        const response = await service.getPastLaunches({
            fields: ["name", "date_utc", "links", "rocket"],
            page: 2,
            sort: { date_unix: -1 },
            query: { upcoming: false }
        });
        expect(response.docs.length).toBeGreaterThan(0);
        expect(response.page).toBe(2);
    });
});
