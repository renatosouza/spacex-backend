import logger from "../../../lib/logger";
import { Launch } from "../../entities/Launch";
import HttpClient from "../../../utils/http";
import { IHttp, RequestException } from "../../../lib/ihttp";
import { cache } from "../../../utils/cache";
import Paginated from "../../entities/Paginated";
import Doc from "../../entities/Doc";

export const SPACEX_API_ENDPOINT = "https://api.spacexdata.com";
export const SPACEX_API_VERSION = "/v4";
export const SPACEX_API_LAUNCHES_PATH = "/launches";
export const SPACEX_API_LAUNCHES_NEXT = "/next";
export const SPACEX_API_LAUNCHES_LATEST = "/latest";
export const SPACEX_API_LAUNCHES_PAST = "/query";
export const SPACEX_API_LAUNCHES_UPCOMING = "/upcoming";

const baseQuery = {
    query: {},
    options: {
        sort: {
            date_unix: -1
        },
        select: ["payloads"],
        populate: [
            {
                path: "payloads",
                select: {
                    launch: 1
                },
                populate: [
                    {
                        path: "launch"
                    }
                ]
            }
        ]
    }
};

const defaultFields = [
    "name",
    "links",
    "rocket",
    "success",
    "date_unix",
    "date_local",
    "upcoming",
    "flight_number"
];

export default class SpaceXService {
    constructor(private httpClient: IHttp = new HttpClient(), protected cacheService = cache) {}

    getNextLaunch(): Promise<Launch> {
        const url = `${SPACEX_API_ENDPOINT}${SPACEX_API_VERSION}${SPACEX_API_LAUNCHES_PATH}${SPACEX_API_LAUNCHES_NEXT}`;
        return this.getFromAPI<Launch>(url);
    }

    getLatestLaunch(): Promise<Launch> {
        const url = `${SPACEX_API_ENDPOINT}${SPACEX_API_VERSION}${SPACEX_API_LAUNCHES_PATH}${SPACEX_API_LAUNCHES_LATEST}`;
        return this.getFromAPI<Launch>(url);
    }

    async getPastLaunches(params?: any): Promise<Paginated<Doc>> {
        const url = `${SPACEX_API_ENDPOINT}${SPACEX_API_VERSION}${SPACEX_API_LAUNCHES_PATH}${SPACEX_API_LAUNCHES_PAST}`;
        const cacheKey = "url" + JSON.stringify(params) || "";
        const cached = await this.cacheService.get<Paginated<Doc>>(cacheKey);

        if (cached) {
            logger.info(`using response from cache for: ${url}`);
            return cached;
        }

        const query = { ...baseQuery };
        query.query = params?.query || { upcoming: false };
        query.options["page"] = params?.page || 1;
        query.options.sort = params?.sort || query.options.sort;
        query.options["limit"] = params?.limit || 10;
        query.options.populate[0].populate[0]["select"] = params?.fields || defaultFields;

        const response = await this.httpClient.post<Paginated<Doc>>(url, query);
        this.cacheService.set(cacheKey, response.data);
        return response.data;
    }

    getUpcomingLaunches(): Promise<Launch[]> {
        const url = `${SPACEX_API_ENDPOINT}${SPACEX_API_VERSION}${SPACEX_API_LAUNCHES_PATH}${SPACEX_API_LAUNCHES_UPCOMING}`;
        return this.getFromAPI<Launch[]>(url);
    }

    private async getFromAPI<T>(url: string): Promise<T> {
        const fromCache = await this.cacheService.get<T>(url);

        if (fromCache) {
            logger.info(`using response from cache for: ${url}`);
            return fromCache;
        }

        try {
            const response = await this.httpClient.get<T>(url);
            this.cacheService.set(url, response.data);
            return response.data;
        } catch (error) {
            throw error as RequestException;
        }
    }

    public extractOnlyNecessaryData(payload: Launch[] = []) {
        const result: Launch[] = [];
        if (payload.length > 0) {
            return payload.map((launch) => ({
                name: launch.name,
                rocket: launch.rocket,
                flightNumber: launch.flight_number,
                dateLocal: launch.date_local,
                smallImages: [...launch.links.flickr.small, launch.links.patch.small],
                largeImages: [...launch.links.flickr.original, launch.links.patch.large],
                youtubeId: launch.links.youtube_id
            }));
        }
        return result;
    }
}
