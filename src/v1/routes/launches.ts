import express, { Request, Response } from "express";
import logger from "../../lib/logger";
import SpaceXService from "../service/spacex-service/spacex_service";
import { getQueryArrayBySplit } from "../../utils/query-utils";
const router = express.Router();

const spaceXService = new SpaceXService();

export default router
    .get("/next", async function (_: Request, res: Response) {
        try {
            const result = await spaceXService.getNextLaunch();
            res.json(spaceXService.extractOnlyNecessaryData([result])[0]);
        } catch (error) {
            logger.error(error);
            res.status(error?.response?.status || 500).json({
                message: error?.response?.data || error?.message
            });
        }
    })
    .get("/latest", async function (_: Request, res: Response) {
        try {
            const result = await spaceXService.getLatestLaunch();
            res.json(spaceXService.extractOnlyNecessaryData([result])[0]);
        } catch (error) {
            logger.error(error);
            handleError(res, error);
        }
    })
    .get("/upcoming", async function (_: Request, res: Response) {
        try {
            const result = await spaceXService.getUpcomingLaunches();
            res.json(spaceXService.extractOnlyNecessaryData(result));
        } catch (error) {
            logger.error(error);
            handleError(res, error);
        }
    })
    .get("/past", async function (req: Request, res: Response) {
        try {
            if (req.query.fields) req.query.fields = getQueryArrayBySplit("fields", req.query);

            const result = await spaceXService.getPastLaunches(req.query);

            let docs = [];
            if (req.query.fields) {
                docs.push(...result.docs.map((doc) => doc.payloads[0]?.launch));
            } else {
                docs.push(
                    ...spaceXService.extractOnlyNecessaryData(
                        result.docs.map((doc) => doc.payloads[0]?.launch)
                    )
                );
            }
            res.json({
                ...result,
                docs
            });
        } catch (error) {
            logger.error(error);
            handleError(res, error);
        }
    });

function handleError(res: express.Response<any, Record<string, any>>, error: any) {
    res.status(error?.response?.status || 500).json({
        message: error?.response?.data || error?.message
    });
}
