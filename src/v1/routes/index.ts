import launches from "./launches";

export default [{ routeName: "/v1/launches", handler: launches }];
