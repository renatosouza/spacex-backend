export interface Fairings {
    reused?: any;
    recovery_attempt?: any;
    recovered?: any;
    ships: any[];
}

export interface Patch {
    small: string;
    large: string;
}

export interface Reddit {
    campaign: string;
    launch?: any;
    media?: any;
    recovery: string;
}

export interface Flickr {
    small: any[];
    original: any[];
}

export interface Links {
    patch: Patch;
    reddit: Reddit;
    flickr: Flickr;
    presskit?: any;
    webcast?: any;
    youtube_id?: any;
    article?: any;
    wikipedia: string;
}

export interface Core {
    core?: any;
    flight?: any;
    gridfins: boolean;
    legs: boolean;
    reused: boolean;
    landing_attempt: boolean;
    landing_success?: any;
    landing_type?: any;
    landpad?: any;
}

export interface Launch {
    fairings: Fairings;
    links: Links;
    static_fire_date_utc?: any;
    static_fire_date_unix?: any;
    net: boolean;
    window?: any;
    rocket: string;
    success?: any;
    failures: any[];
    details?: any;
    crew: any[];
    ships: any[];
    capsules: any[];
    payloads: string[];
    launchpad: string;
    flight_number: number;
    name: string;
    date_utc: Date;
    date_unix: number;
    date_local: Date;
    date_precision: string;
    upcoming: boolean;
    cores: Core[];
    auto_update: boolean;
    tbd: boolean;
    launch_library_id: string;
    id: string;
}
