import { Launch } from "./Launch";

export interface Payload {
    launch: Partial<Launch>;
}

export default interface Doc {
    payloads: Payload[];
}
