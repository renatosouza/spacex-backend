import { configChecker } from "./utils/config-checker";
configChecker();

import SetupServer from "./server";
import routes from "./v1/routes";
import docs from "./v1/docs";

const applicationServer = new SetupServer(process.env.PORT || 3000);
applicationServer.addRoutes(routes);
applicationServer.addDocs(docs);
applicationServer.init();
