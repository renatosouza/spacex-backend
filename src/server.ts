import express from "express";
import path from "path";
import logger from "./lib/logger";
import { Application } from "./entities/ApplicatonInterface";
import morganMiddleware from "./middlewares/morgan-middleware";
import cors from "cors";
import createCache from "./utils/cache";
import swaggerUI from "swagger-ui-express";

export default class SetupServer {
    constructor(private port?: number | string, protected application: Application = express()) {}

    setStaticRoute(): void {
        this.application.use(express.static(path.join(__dirname, "public")));
    }

    getApplication(): Application {
        return this.application;
    }

    private async setup(): Promise<void> {
        this.application.use(cors());
        this.application.use(morganMiddleware);
        this.application.use(express.json());
        this.application.use(express.urlencoded({ extended: false }));
        await createCache();
    }

    async init(): Promise<void> {
        await this.setup();
        this.application.listen(this.port, this.onListening);
        this.application.on("error", this.onError);
    }

    addRoutes(handlers: Array<{ routeName: string; handler: any }>) {
        handlers.forEach((handler) => this.application.use(handler.routeName, handler.handler));
    }

    addDocs(doc: { routeName: string; file: any }) {
        this.application.use(doc.routeName, swaggerUI.serve, swaggerUI.setup(doc.file));
    }

    private onListening = () => {
        logger.info(`Server listening on port: ${this.port}`);
    };

    private onError = (error) => {
        if (error.syscall !== "listen") {
            throw error;
        }

        switch (error.code) {
            case "EACCES":
                console.error(`Port: ${this.port} requires elevated privileges`);
                process.exit(1);
            case "EADDRINUSE":
                console.error(`Port: ${this.port} is already in use`);
                process.exit(1);
            default:
                throw error;
        }
    };
}
