import express from "express";

export interface Application extends express.Application {}
