import { createClient } from "redis";

export class CacheUtil {
    private cacheService = createClient({
        url: `redis://${process.env.REDIS_URL}:${process.env.REDIS_PORT}`
    });
    constructor() {
        this.cacheService.on("error", (error) => {
            console.error(error);
        });
    }

    public async connect(): Promise<void> {
        await this.cacheService.connect();
    }

    public async set(key: string, value: any, ttl: number = 20): Promise<boolean | string> {
        return this.cacheService.setEx(key, ttl, this.stringify(value));
    }

    private stringify(value: any) {
        return "object" === typeof value ? JSON.stringify(value) : "".concat(value);
    }

    public async get<T>(key: string): Promise<T | undefined> {
        const result = await this.cacheService.get(key);
        if (result) {
            return JSON.parse(result) as T;
        }
        return undefined;
    }

    public clear(): void {
        this.cacheService.flushAll();
    }
}

export const cache = new CacheUtil();

export default async function createCache() {
    await cache.connect();
}
