export function getQueryArrayBySplit(key: string, query: any) {
    const result = [];
    if (key) {
        if (query[key]) {
            result.push(...query[key].split(","));
        }
    }
    return result;
}
