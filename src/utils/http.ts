import axios from "axios";
import { IHttp, IHttpResponse, IRequestConfig } from "../lib/ihttp";
export default class HttpClient implements IHttp {
    constructor(protected baseUrl?: string, protected headers?: {}, protected client = axios) {}

    private request<T>(config: IRequestConfig) {
        return this.client.request<T>(config);
    }

    get<T>(url: string, params?: {}, headers?: {}): Promise<IHttpResponse<T>> {
        const finalURL = this.baseUrl || "" + url;
        return this.request<T>({ url: finalURL, params, headers });
    }

    post<T>(url: string, data?: any, params?: {}, headers?: {}): Promise<IHttpResponse<T>> {
        const finalURL = this.baseUrl || "" + url;
        return this.client.post<T>(finalURL, data, {
            params,
            headers: { ...this.headers, ...headers }
        });
    }

    put<T>(url: string, data?: any, params?: {}, headers?: {}): Promise<IHttpResponse<T>> {
        const finalURL = this.baseUrl || "" + url;
        return this.client.put<T>(finalURL, data, {
            params,
            headers: { ...this.headers, ...headers }
        });
    }

    delete<T>(url: string, params?: {}, headers?: {}): Promise<IHttpResponse<T>> {
        const finalURL = this.baseUrl || "" + url;
        return this.client.delete<T>(finalURL, {
            params,
            headers: { ...this.headers, ...headers }
        });
    }
}
