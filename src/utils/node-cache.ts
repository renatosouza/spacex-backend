import NodeCache from "node-cache";

export class CacheUtil {
    constructor(protected cacheService = new NodeCache()) {}

    public async set<T>(key: string, value: any, ttl: number = 20): Promise<boolean> {
        this.cacheService.set(key, value, ttl);
        return Promise.resolve(true);
    }

    public async get<T>(key: string): Promise<T | undefined> {
        return Promise.resolve(this.cacheService.get<T>(key));
    }

    public clear(): void {
        this.cacheService.flushAll();
    }
}

export default new CacheUtil();
