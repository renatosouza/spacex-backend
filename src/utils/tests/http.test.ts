import HttpClient from "../http";

export interface AddressResponse {
    city: string;
    street: string;
    state: string;
    neighborhood: string;
}

describe("HTTP module tests", () => {
    let httpClient: HttpClient;
    beforeEach(() => {
        httpClient = new HttpClient();
    });

    test("should return request data", async () => {
        const resp = await httpClient.get<AddressResponse>(
            "https://brasilapi.com.br/api/cep/v2/38410621",
            null,
            { "content-type": "application/json", accept: "application/json" }
        );
        expect(resp.data.street).toMatch(/Jupiá/gim);
    });

    test("should throw an exception", async () => {
        expect(() =>
            httpClient.get("https://brasilapi.com.br/api/cep/v2/3841062")
        ).rejects.toThrow();
    });
});
