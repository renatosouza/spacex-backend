import { existsSync } from "fs";
import { dirname, join } from "path";
import { FileNotFoundError } from "../lib/errors/FileNotFoundError";

export function configChecker() {
    const appDir = dirname(require.main.filename);
    const configFileAddress = join(appDir, "..", ".env");
    const configFileAddress2 = join(appDir, ".env");

    if (!existsSync(configFileAddress) && !existsSync(configFileAddress2)) {
        require("knoblr").error(
            ".env file was not created, please create this file to start project."
        );
        throw new FileNotFoundError(configFileAddress);
    } else {
        require("dotenv").config({
            silent: true
        });
    }
}
